import asyncio
import logging
import os
import uuid
from io import BytesIO
from PIL import Image
from datetime import datetime
from aiogram import Bot, Dispatcher, types, F
from aiogram.filters import CommandStart
from aiogram.types import Message, CallbackQuery, InlineKeyboardMarkup, InlineKeyboardButton, ReplyKeyboardMarkup, \
    KeyboardButton, InputMediaPhoto, Contact, InputFile, ReplyKeyboardMarkup, KeyboardButton, InputMediaPhoto
from aiogram.utils.chat_action import ChatActionSender, ChatActionMiddleware
from aiogram.utils import markdown
from aiogram.enums import ParseMode, ChatAction
from aiogram.utils.keyboard import ReplyKeyboardBuilder
from aiohttp import ClientSession, ClientConnectorError
import base64

from config import TOKEN

bot = Bot(token=TOKEN)
dp = Dispatcher()

bot = Bot(token=TOKEN)
dp = Dispatcher()

user_data = {}
base_path = "./"
scrapper_url = "http://scrapper_prod:9090"



# Функция для создания клавиатуры для логина
def login_keyboard():
    keyboard = InlineKeyboardMarkup(inline_keyboard=[
        [InlineKeyboardButton(text="Начать авторизацию", callback_data="enter_login")]
    ])
    return keyboard


# Функция для создания клавиатуры для подтверждения номера телефона
def contact_keyboard():
    keyboard = ReplyKeyboardMarkup(
        keyboard=[
            [KeyboardButton(text="Подтвердите номер телефона", request_contact=True)]
        ],
        resize_keyboard=True,
        one_time_keyboard=True
    )
    return keyboard


# Функция для создания перманентной клавиатуры
# Функция для создания перманентной клавиатуры с кнопкой для новой оценки
def permanent_keyboard():
    keyboard = ReplyKeyboardMarkup(
        keyboard=[
            [KeyboardButton(text="Начать новую оценку")]  # Добавляем кнопку новой оценки
        ],
        resize_keyboard=True
    )
    return keyboard


# Функция для создания инлайн-клавиатуры для новой оценки
def inline_assessment_keyboard():
    keyboard = InlineKeyboardMarkup(inline_keyboard=[
        [InlineKeyboardButton(text="Начать новую оценку", callback_data="start_new_assessment")]
    ])
    return keyboard


# Обработчик команды /start
@dp.message(CommandStart())
async def cmd_start(message: Message):
    user_full_name = message.from_user.full_name
    await message.answer(
        f"Здравствуйте, {user_full_name}! Мы рады приветствовать вас на платформе по формированию быстрой и надежной онлайн-оценки автомобиля KAZ_ETALON_GRADE",
        reply_markup=login_keyboard())


# Обработчик нажатия кнопки "Нажмите для запуска аутентификации"
@dp.callback_query(lambda c: c.data == 'enter_login')
async def process_enter_login(callback_query: CallbackQuery):
    await callback_query.message.answer("Введите ваш логин:")
    user_data[callback_query.from_user.id] = {'stage': 'login'}
    await callback_query.answer()


# Обработчик текстового сообщения для логина и пароля
@dp.message(lambda message: message.text and message.from_user.id in user_data)
async def process_login_password(message: Message):
    user_id = message.from_user.id
    user_info = user_data[user_id]

    if user_info.get('stage') == 'login':
        login = message.text.strip()
        # Проверка, существует ли логин
        user_data[user_id]['login'] = login
        print(user_data[user_id].get('login'))
        user_data[user_id]['stage'] = 'password'
        await message.answer("Введите пароль:")

    elif user_info.get('stage') == 'password':
        login = user_data[user_id].get('login')
        password = message.text.strip()
        user_data[user_id]['password'] = password
        # stored_password = str(svet_i_tma.get(login))
        res = await authorize(scrapper_url+"/api/v1/login", user_id)
        if res:
            user_data[user_id]['stage'] = 'contact'
            user_data[user_id]['password'] = password
            user_data[user_id]['login'] = login
            await message.answer('Подтвердите номер телефона.', reply_markup=contact_keyboard())
        else:
            await message.answer('Ошибка авторизации. Попробуйте снова.', reply_markup=login_keyboard())
    if user_info.get('stage') == 'dogovor':
        if user_data[user_id]['dogovor_info_count'] == 0:
            dkp = message.text
            user_data[user_id]['dogovor_num'] = dkp
            user_data[user_id]['dogovor_info_count'] += 1
            await request_sum(message)
        elif user_data[user_id]['dogovor_info_count'] == 1:
            dkp_sum = int(message.text.replace(" ", ""))
            user_data[user_id]['dogovor_sum'] = dkp_sum
            await request_car_photos(message)


# Обработчик контакта пользователя
@dp.message(lambda message: message.contact)
async def process_contact(message: Message):
    contact: Contact = message.contact
    user_id = message.from_user.id
    user_full_name = message.from_user.full_name
    phone_number = contact.phone_number

    user_data[user_id] = {
        'full_name': user_full_name,
        'phone_number': phone_number,
        'stage': 'document_photos',
        'document_photos_count': 0,
        'dogovor_info_count': 0,
        'car_photos_count': 0,
        'login': user_data[user_id].get('login'),
        'password': user_data[user_id].get('password'),

    }
    await message.answer('Аутентификация пройдена успешно ✅', reply_markup=permanent_keyboard())
    await start_assessment(message)


async def start_assessment(message: Message):
    user_id = message.from_user.id

    # Проверка на наличие данных о пользователе
    if user_id not in user_data or 'full_name' not in user_data[user_id]:
        user_data[user_id] = {
            'full_name': message.from_user.full_name,
            'stage': 'document_photos',
            'document_photos_count': 0,
            'car_photos_count': 0,
            'assessment_folder': ''
            # Elaman fixed this error

        }

    timestamp = datetime.now().strftime('%Y-%m-%d_%H-%M-%S')
    assessment_folder = os.path.join(base_path, user_data[user_id]['full_name'], timestamp)
    os.makedirs(assessment_folder, exist_ok=True)
    user_data[user_id]['assessment_folder'] = assessment_folder
    user_data[user_id]['uuid'] = str(uuid.uuid4())

    await bot.send_chat_action(chat_id=message.chat.id, action=ChatAction.UPLOAD_PHOTO)
    # await bot.send_photo(chat_id=message.chat.id, photo=types.FSInputFile(file_primer_1))
    print("Примеры фотографий документов отправлены")
    await message.answer("Загрузите цифровое Удостоверение личности Клиента(Kaspi,  eGov mobile)")


async def request_next_document_photo(message: Message):
    if user_data[message.from_user.id]['document_photos_count'] == 1:
        await message.answer("Отлично✅. Загрузите цифровой ТехПаспорт Автомобиля(Kaspi,  eGov mobile)")
        # await bot.send_photo(chat_id=message.chat.id, photo=types.FSInputFile(file_primer_2))


async def request_dkp(message: Message):
    if user_data[message.from_user.id]['document_photos_count'] == 2:
        await message.answer("Отлично✅. Укажите номер и дату Договора залога, например:  Nº234234324234 от 23.06.2024")
        # await bot.send_photo(chat_id=message.chat.id, photo=types.FSInputFile(file_primer_2))


async def request_sum(message: Message):
    if user_data[message.from_user.id]['dogovor_info_count'] == 1:
        await message.answer("Отлично✅. Укажите сумму продажи автомобиля в соответствии с ДКП(без пробелов и посторонних знаков)")


# Запрос первой фотографии автомобиля
async def request_car_photos(message: Message):
    user_id = message.from_user.id
    user_data[user_id]['stage'] = 'car_photos'
    user_data[user_id]['car_photos_count'] = 0  # ПРОБУЮ ВАРИАНТ!!!!!!!!!!!!!!!!!!!

    await bot.send_chat_action(chat_id=message.chat.id, action=ChatAction.UPLOAD_PHOTO)

    await message.answer('''Отлично✅. Загрузите фото Автомобиля спереди''')
    await bot.send_photo(chat_id=message.chat.id, photo=types.FSInputFile('front.jpg'))
    print("Примеры фотографий автомобиля отправлены")


async def request_next_car_photo(message: Message):
    car_photos_count = user_data[message.from_user.id]['car_photos_count']
    if car_photos_count == 1:
        await message.answer("Отлично✅. Загрузите фото Автомобиля сзади")
        await bot.send_photo(chat_id=message.chat.id, photo=types.FSInputFile('back.jpg'))
    elif car_photos_count == 2:
        await message.answer("Отлично✅. Загрузите фото Автомобиля слева")
        await bot.send_photo(chat_id=message.chat.id, photo=types.FSInputFile('right.jpg'))
    elif car_photos_count == 3:
        await message.answer("Отлично✅. Загрузите фото Автомобиля справа")
        await bot.send_photo(chat_id=message.chat.id, photo=types.FSInputFile('left.jpg'))
    elif car_photos_count == 4:
        await message.answer("Отлично✅. Загрузите фото салона Автомобиля")
        await bot.send_photo(chat_id=message.chat.id, photo=types.FSInputFile('interior.jpg'))
    elif car_photos_count == 5:
        await message.answer("Отлично✅. Загрузите фото VIN с кузова Автомобиля")
        await bot.send_photo(chat_id=message.chat.id, photo=types.FSInputFile('vin.jpg'))


@dp.message(F.document.mime_type == 'application/pdf')
async def handle_pdf(message: Message):
    user_id = message.from_user.id
    user_full_name = message.from_user.full_name
    stage = user_data.get(user_id).get('stage')
    login = user_data.get(user_id).get('login')
    password = user_data.get(user_id).get('password')
    print(stage, login, password, "check")
    if stage == 'document_photos':
        if user_data[user_id]['document_photos_count'] == 0:
            await send_document_base64(user_data[user_id]['uuid'], 'udv', message.document,
                                       scrapper_url+'/api/v1/upload', login, password)
        if user_data[user_id]['document_photos_count'] == 1:
            await send_document_base64(user_data[user_id]['uuid'], 'srts', message.document,
                                       scrapper_url+'/api/v1/upload', login, password)
        user_data[user_id]['document_photos_count'] += 1
        if user_data[user_id]['document_photos_count'] == 2:
            user_data[user_id]['stage'] = 'dogovor'
            await request_dkp(message)
        else:
            await request_next_document_photo(message)


@dp.message(F.photo)
async def handle_photos(message: Message):
    user_id = message.from_user.id
    user_full_name = message.from_user.full_name
    stage = user_data.get(user_id, {}).get('stage')
    if stage == 'document_photos':
        await message.answer("Ошибка: ожидается файл в формате PDF. Пожалуйста, загрузите документ в формате PDF.")
    if stage == 'car_photos':
        login = user_data.get(user_id).get('login')
        password = user_data.get(user_id).get('password')
        if user_data[user_id]['car_photos_count']==5:
            await send_photo_base64(user_data[user_id]['uuid'], 'vin', message.photo[-1],
                                scrapper_url+'/api/v1/upload', login, password)
            user_data[user_id]['car_photos_count'] += 1
        else:
            await send_photo_base64(user_data[user_id]['uuid'], 'car', message.photo[-1],
                                    scrapper_url+'/api/v1/upload', login, password)
            user_data[user_id]['car_photos_count'] += 1

        if user_data[user_id]['car_photos_count'] == 6:
            user_data[user_id]['stage'] = 'completed'


            await message.answer(f'''
            Благодарим вас за использование платформы. 
            Материалы для оценки приняты в обработку ✅
''', reply_markup=inline_assessment_keyboard())
            await signal_to_generate(scrapper_url+"/api/v1/generate", user_id)
        # else:
        # await request_car_photos(message)
        elif stage != 'completed':
            await request_next_car_photo(message)


# а вот и функция новой оценки подъехала #
@dp.callback_query(lambda c: c.data == 'start_new_assessment')
async def process_restart(callback_query: CallbackQuery):
    user_id = callback_query.from_user.id
    user_full_name = callback_query.from_user.full_name

    if user_id not in user_data:
        user_data[user_id] = {
            'full_name': user_full_name,
            'stage': 'document_photos',
            'document_photos_count': 0,
            'dogovor_info_count': 0,
            'car_photos_count': 0,
            'assessment_folder': '',
            'uuid': '',
            'login': user_data[user_id].get('login'),
            'password': user_data[user_id].get('password')
        }
    else:
        user_data[user_id].update({
            'stage': 'document_photos',
            'document_photos_count': 0,
            'dogovor_info_count': 0,
            'car_photos_count': 0,
            'assessment_folder': '',
            'uuid': str(uuid.uuid4()),
            'login': user_data[user_id].get('login'),
            'password': user_data[user_id].get('password'),
        })
    print(f"process_restart called for user_id: {user_id}")
    print(f"user_data before new assessment: {user_data}")

    await start_assessment(callback_query.message)
    await callback_query.answer()

async def send_photo_base64(sessionUUID, photoType, photo: types.PhotoSize, api_url: str, login, password):
    try:
        file_id = photo.file_id
        file = await bot.get_file(file_id)
        file_path = file.file_path

        # Загружаем файл
        file_bytes_io = await bot.download_file(file_path)
        file_bytes_io.seek(0)  # Убедитесь, что указатель находится в начале файла
        file_bytes = file_bytes_io.read()
        # Сжимаем изображение на 50% и конвертируем в PNG
        image = Image.open(BytesIO(file_bytes))
        new_size = (image.width // 2, image.height // 2)
        compressed_image = image.resize(new_size, Image.Resampling.LANCZOS)

        output_io = BytesIO()
        compressed_image.save(output_io, format='PNG')
        output_io.seek(0)
        compressed_image_bytes = output_io.read()

        # Кодируем файл в base64
        photo_base64 = base64.b64encode(compressed_image_bytes).decode('utf-8')
        # Кодируем файл в base64
        print(login, password, "фото")
        credentials = f"{login}:{password}"
        encoded_credentials = base64.b64encode(credentials.encode('utf-8')).decode('utf-8')
        authorization_header = f"Basic {encoded_credentials}"

        # Отправляем base64 в API
        async with ClientSession() as session:
            headers = {'Content-Type': 'application/json',
                       'Authorization': authorization_header}
            payload = {
                'uuid': str(sessionUUID),
                'picture': str(photo_base64),
                'type': str(photoType),
                'ext': '.png'
            }
            async with session.post(api_url, json=payload, headers=headers) as response:
                if response.status == 200:
                    print("Фото успешно отправлено в API")
                else:
                    print(f"Ошибка при отправке фото в API: {response.status}")
    except ClientConnectorError as e:
        print(f"Ошибка подключения: {e}")
    except Exception as e:
        print(f"Произошла ошибка: {e}")


async def send_document_base64(sessionUUID, documentType, document: types.Document, api_url: str, login, password):
    try:
        print(sessionUUID)
        file_id = document.file_id
        file = await bot.get_file(file_id)
        file_path = file.file_path

        # Загружаем файл
        file_bytes_io = await bot.download_file(file_path)
        file_bytes_io.seek(0)  # Убедитесь, что указатель находится в начале файла
        file_bytes = file_bytes_io.read()
        # Кодируем файл в base64
        document_base64 = base64.b64encode(file_bytes).decode('utf-8')
        print(login, password, "фото")
        credentials = f"{login}:{password}"
        encoded_credentials = base64.b64encode(credentials.encode('utf-8')).decode('utf-8')
        authorization_header = f"Basic {encoded_credentials}"

        # Отправляем base64 в API
        async with ClientSession() as session:
            headers = {'Content-Type': 'application/json',
                       'Authorization': authorization_header}
            payload = {
                'uuid': str(sessionUUID),
                'picture': str(document_base64),
                'type': str(documentType),
                'ext': '.pdf'
            }
            async with session.post(api_url, json=payload, headers=headers) as response:
                if response.status == 200:
                    print("Документ успешно отправлен в API")
                else:
                    print(f"Ошибка при отправке документа в API: {response.status}")
    except ClientConnectorError as e:
        print(f"Ошибка подключения: {e}")
    except Exception as e:
        print(f"Произошла ошибка: {e}")


async def authorize(api_url: str, user_id: str) -> bool:
    try:
        # Извлекаем данные пользователя и кодируем их в base64
        credentials = f"{user_data[user_id].get('login')}:{user_data[user_id].get('password')}"
        print(user_data[user_id].get('login'), user_data[user_id].get('password'))
        encoded_credentials = base64.b64encode(credentials.encode('utf-8')).decode('utf-8')
        authorization_header = f"Basic {encoded_credentials}"

        # Отправляем запрос в API
        async with ClientSession() as session:
            headers = {'Authorization': authorization_header}
            async with session.post(api_url, json=None, headers=headers) as response:
                if response.status == 200:
                    user_data[user_id]['password'] = user_data[user_id].get('password')
                    user_data[user_id]['login'] = user_data[user_id].get('login')
                    print("Успешный логин", user_data[user_id]['login'], user_data[user_id]['password'])
                    return True
                else:
                    print(f"Проблемка: {response.status}")
                    return False
    except ClientConnectorError as e:
        print(f"Ошибка подключения: {e}")
        return False
    except Exception as e:
        print(f"Произошла ошибка: {e}")
        return False


async def signal_to_generate(api_url: str, user_id: str) -> bool:
    try:
        # Извлекаем данные пользователя и кодируем их в base64
        credentials = f"{user_data[user_id].get('login')}:{user_data[user_id].get('password')}"
        print(user_data[user_id].get('login'), user_data[user_id].get('password'))
        encoded_credentials = base64.b64encode(credentials.encode('utf-8')).decode('utf-8')
        authorization_header = f"Basic {encoded_credentials}"

        # Отправляем запрос в API
        async with ClientSession() as session:
            headers = {'Content-Type': 'application/json',
                       'Authorization': authorization_header}
            payload = {
                'id': str(user_data[user_id].get('uuid')),
                'pledge': user_data[user_id].get('dogovor_num'),
                'sale_price': user_data[user_id].get('dogovor_sum')
            }
            async with session.post(api_url, json=payload, headers=headers, timeout=120) as response:
                if response.status == 200:
                    user_data[user_id]['password'] = user_data[user_id].get('password')
                    user_data[user_id]['login'] = user_data[user_id].get('login')
                    print("Успешный логин", user_data[user_id]['login'], user_data[user_id]['password'])
                    return True
                else:
                    print(f"Проблемка: {response.status}")
                    return False
    except ClientConnectorError as e:
        print(f"Ошибка подключения: {e}")
        return False
    except Exception as e:
        print(f"Произошла ошибка: {e}")
        return False

# Обработчик для кнопки "Начать новую оценку"
@dp.message(lambda message: message.text == "Начать новую оценку")
async def handle_new_assessment(message: Message):
    user_id = message.from_user.id
    user_full_name = message.from_user.full_name

    # Сбрасываем данные пользователя
    user_data[user_id] = {
        'full_name': user_full_name,
        'stage': 'document_photos',
        'document_photos_count': 0,
        'dogovor_info_count': 0,
        'car_photos_count': 0,
        'assessment_folder': '',
        'uuid': str(uuid.uuid4()),
    }

    await message.answer("Вы начали новую оценку. Пожалуйста, следуйте инструкциям для загрузки документов и фото.")
    await start_assessment(message)  # Запускаем новый процесс оценки

async def main():
    logging.basicConfig(level=logging.INFO)
    if not os.path.exists(base_path):
        os.makedirs(base_path)
    await bot.delete_webhook(drop_pending_updates=True)
    await dp.start_polling(bot)


if __name__ == '__main__':
    try:
        asyncio.run(main())
    except KeyboardInterrupt:
        print('Exit')
